package yamldiff

import (
	"bytes"
	"errors"
	"fmt"
)

type CompareError []string

var (
	_ error = CompareError{}
)

func (e *CompareError) Append(s string) {
	*e = append(*e, s)
}

func (e CompareError) Error() string {
	if len(e) == 0 {
		return ""
	}

	var errs []error
	for _, s := range e {
		errs = append(errs, errors.New(s))
	}
	return errors.Join(errs...).Error()
}

func (e *CompareError) String() string {
	return e.formatString("%s")
}

func (e *CompareError) StringIndent(indent string) string {
	return e.formatString(indent+"%s")
}

func (e CompareError) formatString(fs string) string {
	var sb bytes.Buffer

	fmts := []string{
		fs,
		"\n"+fs,
	}
	var findex int
	for _, s := range e {
		if findex == 1 || sb.Len() > 0 {
			findex = 1
		} 
		fmt.Fprintf(&sb, fmts[findex], s)
	}
	return sb.String()
}

