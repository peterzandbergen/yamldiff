package yamldiff

import (
	"embed"
	"io/fs"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

//go:embed resources
var resources embed.FS

func TestCompare(t *testing.T) {
	dir, err := fs.Sub(resources, "resources")
	assert.NoError(t, err)

	// Read the file
	d1, err := fs.ReadFile(dir, "resources1.yaml")
	require.NoError(t, err)
	// Read the file
	d2, err := fs.ReadFile(dir, "resources2.yaml")
	assert.NoError(t, err)

	rs1, err := LoadResources(d1)
	assert.NoError(t, err)

	rs2, err := LoadResources(d2)
	assert.NoError(t, err)

	err = CompareResourceMaps(rs1, rs2)
	assert.NoError(t, err)
}

func TestCompareError(t *testing.T) {
	dir, err := fs.Sub(resources, "resources")
	assert.NoError(t, err)

	// Read the file
	d1, err := fs.ReadFile(dir, "resources3.yaml")
	require.NoError(t, err)
	// Read the file
	d2, err := fs.ReadFile(dir, "resources4.yaml")
	assert.NoError(t, err)

	rs1, err := LoadResources(d1)
	assert.NoError(t, err)

	rs2, err := LoadResources(d2)
	assert.NoError(t, err)

	err = CompareResourceMaps(rs1, rs2)
	var te1 ErrNotEqual
	assert.ErrorAs(t, err, &te1)

	var te2 ErrMissing
	assert.ErrorAs(t, err, &te2)
}

func TestCompareErrorAppend(t *testing.T) {
	ce := &CompareError{}
	ce.Append("error")
	ce.Append("error1")

	assert.Equal(t, ce.String(), "error\nerror1")
	assert.Equal(t, ce.Error(), "error\nerror1")
	assert.Equal(t, ce.StringIndent("  "), "  error\n  error1")
}