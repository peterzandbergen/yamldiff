package yamldiff

import (
	"errors"
	"io/fs"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	// "github.com/stretchr/testify/require"
)

func TestUnmarshalMulti(t *testing.T) {
	type result struct {
		l   int
		err error
	}
	cases := []struct {
		name     string
		args     string
		want     result
		validate func(want, got result, t *testing.T)
	}{
		{
			name: "one any",
			args: `name: peter`,
			want: result{l: 1, err: nil},
			validate: func(want, got result, t *testing.T) {
				assert.Equal(t, want, got)
			},
		},
		{
			name: "two any",
			args: `
name: peter
---
name: spock
---
---`,
			want: result{l: 2, err: nil},
			validate: func(want, got result, t *testing.T) {
				assert.Equal(t, want, got)
			},
		},
	}

	for _, cc := range cases {
		t.Run(cc.name, func(tt *testing.T) {
			var a []any
			err := UnmarshalMulti([]byte(cc.args), &a)
			got := result{l: len(a), err: err}
			cc.validate(cc.want, got, t)
		})
	}
}

func TestUnmarshalResource(t *testing.T) {
	type result struct {
		resource []Resource
	}
	cases := []struct {
		name     string
		args     string
		want     result
		validate func(want, got result, t *testing.T)
	}{
		{
			name: "one one",
			args: `
apiVersion: v1
kind: Pod
metadata:
  name: pod-1
spec:
  lala: tralala

---
apiVersion: v1
kind: Pod
metadata:
  name: pod-2
spec:
  lala: tralala

`,
			want: result{
				resource: []Resource{
					{
						ApiVersion: "v1",
						Kind:       "Pod",
						Metadata: Metadata{
							Name: "pod-1",
						},
						Spec: map[string]any{
							"lala": "tralala",
						},
					},
					{
						ApiVersion: "v1",
						Kind:       "Pod",
						Metadata: Metadata{
							Name: "pod-2",
						},
						Spec: map[string]any{
							"lala": "tralala",
						},
					},
				},
			},
			validate: func(want, got result, t *testing.T) {
				assert.Equal(t, want, got)
			},
		},
		{
			name: "empty spec",
			args: `
apiVersion: v1
kind: Pod
metadata:
    name: pod-1

`,
			want: result{
				resource: []Resource{
					{
						ApiVersion: "v1",
						Kind:       "Pod",
						Metadata: Metadata{
							Name: "pod-1",
						},
					},
				},
			},
			validate: func(want, got result, t *testing.T) {
				assert.Equal(t, want, got)
			},
		},
	}

	for _, cc := range cases {
		t.Run(cc.name, func(tt *testing.T) {
			var got result
			err := UnmarshalMulti([]byte(cc.args), &got.resource)
			assert.NoError(tt, err)
			cc.validate(cc.want, got, t)
		})
	}
}

func TestLoadResoucesFile(t *testing.T) {
	const filename = "/home/peza/DevProjects/yamldiff/test.yaml"
	rsc, err := LoadResourcesFile(filename)
	_ = rsc
	_ = err
	t.Logf("done")
}

func TestErrors(t *testing.T) {
	err1 := ErrMissing("hallo")
	err2 := ErrNotEqual("jammer")

	var targetErr1 ErrMissing
	var targetErr2 ErrNotEqual

	if !errors.As(err1, &targetErr1) {
		t.Errorf("error")
	}
	if !errors.As(err2, &targetErr2) {
		t.Errorf("error")
	}
}

func TestTrimGenerated(t *testing.T) {
	res := TrimGenerated("hallo-daar", "-")
	assert.Equal(t, "hallo", res)

	res = TrimGenerated("hallo-allemaal-daar", "-")
	assert.Equal(t, "hallo-allemaal", res)
}

func TestRemoveGenerated(t *testing.T) {
	dir, err := fs.Sub(resources, "resources")
	assert.NoError(t, err)

	// Read the file
	d1, err := LoadResourcesFS(dir, "resources-generated-1.yaml")
	require.NoError(t, err)

	kinds := []string{"ConfigMap", "Secret"}
	removed := RemoveGenerated(d1, kinds)
	t.Log(removed)
}
