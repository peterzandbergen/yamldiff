# Ydiff and Yprint

This project implements cli tools that can be used to compair Yaml manifests for Kubernetes deployments. 
The main focus is to check for equality of resource definitions from Kustomize to test if modifications in the directory structure are done correctly and do not change the resulting Yaml files.

## Usage ydiff

ydiff <file1> <file2>

This command compares the Kubernetes resources in file1 with the ones defined in file2.
It shows the resources missing in one or the other file and the resources that are not equal.

The exit code indicates the result
- 0: files are equal
- 1: both files contain the same resurces but some resources are different
- 2: file2 does not contain all resources from file1
- 4: file1 does not contain all resources from file2
- 6: both files miss resources


