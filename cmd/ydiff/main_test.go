package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseConfig(t *testing.T) {
	cases := []struct {
		name     string
		args     []string
		cfg      *config
		err      error
		validate func(want, got *config, t *testing.T)
	}{
		{
			name: "correct call",
			args: []string{
				"diff",
				"file1",
				"file2",
			},
			cfg: &config{
				in1: "file1",
				in2: "file2",
				keepGenerated: false,
			},
		},
		{
			name: "missing files",
			args: []string{
				"diff",
				"file1",
			},
			validate: func(want, got *config, t *testing.T) {
			},
			err: ErrMissingArguments,
		},
		{
			name: "keep generated",
			args: []string{
				"diff",
				"-keep-generated",
				"file1",
				"file2",
			},
			cfg: &config{
				in1: "file1",
				in2: "file2",
				keepGenerated: true,
			},
		},
	}
	for _, cc := range cases {
		t.Run(cc.name, func(tt *testing.T) {
			cfg, err := parseConfig(cc.args)
			assert.ErrorIs(tt, err, cc.err)
			assert.Equal(tt, cc.cfg, cfg)
		})
	}
}
