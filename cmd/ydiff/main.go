package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/peterzandbergen/yamldiff"
)

type config struct {
	in1 string
	in2 string

	keepGenerated bool
}

var (
	ErrMissingArguments = errors.New("missing arguments")
)

func parseConfig(args []string) (*config, error) {
	var keepGenerated bool
	arg0 := "missing"
	if len(args) > 0 {
		arg0 = filepath.Base(args[0])
	}
	fs := flag.NewFlagSet("ydiff", flag.ContinueOnError)
	fs.Usage = func() {
		fmt.Fprintf(fs.Output(), "Usage of %s\n", arg0)
		fmt.Fprintf(fs.Output(), "    %s file1 file2\n", arg0)
		fs.PrintDefaults()
	}
	fs.BoolVar(&keepGenerated, "keep-generated", false, "set to true to keep the generated hash for secrets and configmaps")

	if len(args) < 3 {
		fs.Usage()
		return nil, ErrMissingArguments
	}
	args = args[1:]

	err := fs.Parse(args)
	if err != nil {
		fs.Usage()
		return nil, err
	}

	if fs.NArg() != 2 {
		fs.Usage()
		return nil, ErrMissingArguments
	}

	return &config{
		in1: fs.Args()[0],
		in2: fs.Args()[1],
		keepGenerated: keepGenerated,
	}, nil
}

func compare(r1, r2 map[string]yamldiff.Resource) error {
	return yamldiff.CompareResourceMaps(r1, r2)
}

// run expects args as delivered by os.Args
func run(args []string) error {
	cfg, err := parseConfig(args)
	if err != nil {
		return err
	}

	// Load files
	r1, err := yamldiff.LoadResourcesFile(cfg.in1)
	if err != nil {
		return err
	}

	r2, err := yamldiff.LoadResourcesFile(cfg.in2)
	if err != nil {
		return err
	}

	if !cfg.keepGenerated {
		kinds := []string{"ConfigMap", "Secret"}
		r1 = yamldiff.RemoveGenerated(r1, kinds)
		r2 = yamldiff.RemoveGenerated(r2, kinds)
	}

	return compare(r1, r2)
}

func main() {
	if err := run(os.Args); err != nil {
		fmt.Printf("error: \n%s\n", err)
		os.Exit(100)
	}
}
