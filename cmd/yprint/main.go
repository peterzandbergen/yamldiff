package main

import (
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/peterzandbergen/yamldiff"

	"gopkg.in/yaml.v3"
)

type config struct {
	in string
}

var (
	ErrMissingArguments = errors.New("missing arguments")
)

func parseConfig(args []string) (*config, error) {
	arg0 := "missing"
	if len(args) > 0 {
		arg0 = args[0]
	}
	fs := flag.NewFlagSet("ydiff", flag.ContinueOnError)
	fs.Usage = func() {
		fmt.Fprintf(fs.Output(), "Usage of %s\n", arg0)
		fmt.Fprintf(fs.Output(), "    %s file1\n", arg0)
		fs.PrintDefaults()
	}

	if len(args) < 2 {
		fs.Usage()
		return nil, fmt.Errorf("less than 2 arguments: %w", ErrMissingArguments)
	}
	args = args[1:]

	err := fs.Parse(args)
	if err != nil {
		fs.Usage()
		return nil, fmt.Errorf("parse failed: %w", err)
	}

	if fs.NArg() != 1 {
		fs.Usage()
		return nil, ErrMissingArguments
	}

	return &config{
		in: fs.Args()[0],
	}, nil
}

func run(args []string) error {
	cfg, err := parseConfig(args)
	if err != nil {
		return err
	}
	if err := loadAndPrint(cfg.in); err != nil {
		return err
	}
	return nil
}

func loadAndPrint(name string) error {
	res, err := yamldiff.LoadResourcesFile(name)
	if err != nil {
		return fmt.Errorf("failed to load resources file: %w", err)
	}
	if len(res) == 0 {
		return fmt.Errorf("no resources returned")
	}
	for k, v := range res {
		b, err := yaml.Marshal(v)
		if err != nil {
			continue
		}
		fmt.Printf("---\n# %s\n%s\n", k, string(b))
	}
	return nil
}

func main() {
	if err := run(os.Args); err != nil {
		fmt.Printf("error running run: %s\n", err)
	}
}
