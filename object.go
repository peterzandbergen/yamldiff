package yamldiff

import (
	"bytes"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"reflect"
	"strings"

	"gopkg.in/yaml.v3"
)

type Metadata struct {
	Annotations map[string]string `json:"annotations" yaml:"annotations"`
	Labels      map[string]string `json:"labels" yaml:"labels"`
	Name        string            `json:"name" yaml:"name"`
	Namespace   string            `json:"namespace" yaml:"namespace"`
}

type Resource struct {
	ApiVersion string         `json:"apiVersion" yaml:"apiVersion"`
	Kind       string         `json:"kind" yaml:"kind"`
	Metadata   Metadata       `json:"metadata" yaml:"metadata"`
	Spec       map[string]any `json:"spec" yaml:"spec"`
}

type ErrNotEqual string

func (e ErrNotEqual) Error() string { return string(e) }

type ErrMissing string

func (e ErrMissing) Error() string { return string(e) }

func UnmarshalMulti[T any](b []byte, res *[]T) error {
	parts := bytes.Split(b, []byte("---"))
	if len(parts) == 0 {
		return errors.New("no parts found")
	}
	for i, bb := range parts {
		var a *T
		err := yaml.Unmarshal(bb, &a)
		if err != nil {
			return fmt.Errorf("error unmarshalling element %d: %w", i, err)
		}
		if a == nil {
			continue
		}
		*res = append(*res, *a)
	}
	return nil
}

func (r Resource) Key() string {
	return r.Metadata.Namespace + "." + r.Metadata.Name
}

func UnmarshalResources(b []byte) (map[string]Resource, error) {
	var rsc []Resource
	if err := UnmarshalMulti(b, &rsc); err != nil {
		return nil, err
	}

	if len(rsc) == 0 {
		return nil, errors.New("no resouces returned")
	}

	// Build the map
	res := make(map[string]Resource)
	for _, r := range rsc {
		res[r.Key()] = r
	}
	return res, nil
}

func LoadResources(data []byte) (map[string]Resource, error) {
	res, err := UnmarshalResources(data)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func LoadResourcesFS(fss fs.FS, name string) (map[string]Resource, error) {
	data, err := fs.ReadFile(fss, name)
	if err != nil {
		return nil, err
	}
	return LoadResources(data)
}

func LoadResourcesFile(name string) (map[string]Resource, error) {
	data, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	return LoadResources(data)
}

func CompareResources(res1, res2 Resource) int {
	if reflect.DeepEqual(res1, res2) {
		return 0
	}
	return -1
}

func CompareResourceMaps(m1, m2 map[string]Resource) error {
	// Build a map with all entries.
	all := make(map[string]struct{}, len(m1)+len(m2))
	for k := range m1 {
		all[k] = struct{}{}
	}
	for k := range m2 {
		all[k] = struct{}{}
	}

	var errs []error
	for k := range all {
		r1, ok := m1[k]
		if !ok {
			err := ErrMissing(fmt.Sprintf("%s %s not found in first map", m2[k].Kind, k))
			errs = append(errs, err)
			continue
		}
		r2, ok := m2[k]
		if !ok {
			err := ErrMissing(fmt.Sprintf("%s %s not found in second map", m1[k].Kind, k))
			errs = append(errs, err)
			continue
		}
		if CompareResources(r1, r2) != 0 {
			err := ErrNotEqual(fmt.Sprintf("%ss %s are not equal", m1[k].Kind, k))
			errs = append(errs, err)
			continue
		}
	}
	if len(errs) > 0 {
		return errors.Join(errs...)
	}
	return nil
}

// TrimGenerated removes the generated id.
func TrimGenerated(s string, separator ...string) string {
	sep := "-"
	if len(separator) >0 {
		sep = separator[0]
	}
	i := strings.LastIndex(s, sep)
	if i < 0 {
		return s
	}
	return s[:i]
}

func RemoveGenerated(resources map[string]Resource, kinds []string) map[string]Resource {
	res := make(map[string]Resource, len(resources))

	for _, r := range resources {
		for _, kind := range kinds {
			if r.Kind == kind {
				r.Metadata.Name = TrimGenerated(r.Metadata.Name)
				break
			}
		}
		res[r.Key()] = r
	}
	return res
}
